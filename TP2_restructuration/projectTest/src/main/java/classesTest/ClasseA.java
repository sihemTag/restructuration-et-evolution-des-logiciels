package classesTest;

import interfaces.Iinterface;



public class ClasseA implements Iinterface {
	private int attr1;
    private String attr2;    
    private ClasseC attr3; 
    private ClasseB t1;
    
    public ClasseA() {
        this.attr1 = 15;
        this.attr2 = "B+";
        this.attr3 = new ClasseC();
        t1 = new ClasseB();
    }

    public void printSwitchTest() {
        switch (this.attr2) {
            case "B+":
                System.out.println(this.attr2);
                break;
            default:
                System.out.println("BAD");
                break;
        }
        t1.printSwitchTest();
    }
    

    public ClasseB getT1() {
		return t1;
	}

	public void setT1(ClasseB t1) {
		this.t1 = t1;
	}

	public ClasseC getAttr3() {
		return attr3;
	}

	public void setAttr3(ClasseC attr3) {
		this.attr3 = attr3;
	}

	//    @Override public String toString()
    @Override
    public String toString() {
        return super.toString();
    }
}