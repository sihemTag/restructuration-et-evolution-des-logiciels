package models;

public class Product {
	private String name;
	private float price;
	private String expirationDate;
	
	public Product(String name, float price, String expirationDate) {
		super();
		this.name = name;
		this.price = price;
		this.expirationDate = expirationDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public String getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}
	
	

}
