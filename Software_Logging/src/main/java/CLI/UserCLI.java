package CLI;

import java.io.BufferedReader;




import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Scanner;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import org.bson.Document;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import application.App;
import application.JsonFormatteur;
import application.UserContexte;
import models.Product;
import models.User;


public class UserCLI extends AbstractMain{
	
	public static IntegerInputProcessor inputProcessor;
	public static MongoClient mongoClient = MongoClients.create("mongodb://localhost:27017");
	public static MongoDatabase database = mongoClient.getDatabase("userApplication");
    public static MongoCollection<Document> userCollection = database.getCollection("user");
    public static MongoCollection<Document> productCollection = database.getCollection("product");
    
    private final static Logger readingLogger = Logger.getLogger("ReadAction");
    private final static Logger writingLogger = Logger.getLogger("WriteAction");
    private final static Logger priceLogger = Logger.getLogger("Searching_expensive_product_Action");
	
	@SuppressWarnings("resource")
	public static void main(String[] args)  {
		BufferedReader inputReader;
		String userInput = "";
		UserCLI main = new UserCLI();
		
        //configuration des loggers
		setupReadingLogger();
		setupWritingLogger();
		setupPriceLogger();
              		
		try {
			inputReader = new BufferedReader(new InputStreamReader(System.in));
			//login
			System.out.println("Please login first");
			Scanner sc = new Scanner(System.in);
			System.out.println("UserName");
			String email = sc.nextLine().trim();
			System.out.println("Password");
			String password = sc.nextLine().trim();
			if(!App.existUser(email, userCollection)) {
				System.err.println("user not found, please sign in first!");
				sc = new Scanner(System.in);
				//name
				System.out.println("insert name:");
				String name = sc.nextLine().trim();
				//age
				System.out.println("insert age:");
				inputProcessor = new IntegerInputProcessor(inputReader);
				int age = inputProcessor.process();
				//username
				System.out.println("insert UserName:");
				String username = sc.nextLine().trim();
				//name
				System.out.println("insert password:");
				String mdp = sc.nextLine().trim();
				createUser(name, username, mdp, age);
				
				//afficher le menu a l'utilisateur
				do {
					main.menu();
					userInput = inputReader.readLine();
					main.processUserInput(inputReader,userInput,userCollection,productCollection,email);
					Thread.sleep(2000);
				} while(!userInput.equals(QUIT));
			}else {
				//afficher le menu a l'utilisateur
				do {
					main.menu();
					userInput = inputReader.readLine();
					main.processUserInput(inputReader,userInput,userCollection,productCollection,email);
					Thread.sleep(2000);
				} while(!userInput.equals(QUIT));
			}						
		}catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}			
	}

	@Override
	protected void menu() {
		StringBuilder builder = new StringBuilder();
		builder.append(QUIT+". Quit");
		builder.append("\n1. Display all products.");
		builder.append("\n2. Add a new product.");
		builder.append("\n3. Fetch a product by its ID.");		
		builder.append("\n4. Delete a product by its ID.");
		builder.append("\n5. Update a product’s info.");
		
		System.out.println(builder);
	}
	
	private void processUserInput(BufferedReader reader, String userInput,MongoCollection<Document> userCollection,MongoCollection<Document> productCollection, String email) {
		String name, expirationDate, id;
		float price;
		Scanner scanner;
		
		switch(userInput) {
		
		case QUIT :
			System.out.println("A bientôt!");
			return;
		case "1":
			System.out.println("All available products :");
			App.displayProducts(productCollection);
			UserContexte.setUsername(email);
			readingLogger.info("Display products");
			break;
		case "2": 
			System.out.println("Please enter the name of the product :");
			scanner = new Scanner(System.in);
			name = scanner.nextLine().trim();
			
			System.out.println("Please enter the price of the product :");
			inputProcessor = new IntegerInputProcessor(reader);
			try {
				price = inputProcessor.process();
				
				System.out.println("Please enter the expiration date of the product :");
				expirationDate = scanner.nextLine().trim();
				
				Product product = new Product(name, price, expirationDate);
				App.insertProduct(product, productCollection);
				System.out.println("Product successfully added");
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			UserContexte.setUsername(email);
			writingLogger.info("Add a new product");
			
			break;			
		case "3":
			System.out.println("Please enter the id of the product you want to see :");
			scanner = new Scanner(System.in);
			id = scanner.nextLine().trim();
			App.getProductById(id, productCollection);
			UserContexte.setUsername(email);
			readingLogger.info("getProductById");
			
			List<Document> mostExpensiveProducts = App.getTopExpensiveProducts(5, productCollection);
			for(Document doc : mostExpensiveProducts) {
				String docId = doc.getObjectId("_id").toString();

			    if (id.equals(docId)) {
			    	UserContexte.setUsername(email);
					priceLogger.info("search_expensive_prod");			        
			        break; 
			    }
				
			}
			break;
		case "4":
			System.out.println("Please enter the id of the product you want to delete :");
			scanner = new Scanner(System.in);
			id = scanner.nextLine().trim();
			App.deleteProductById(id, productCollection);
			UserContexte.setUsername(email);
			writingLogger.info("Delete product");
			System.out.println("Product deleted successfully!");
			break;
		case "5":
			System.out.println("Please enter the id of the product you want to update :");
			scanner = new Scanner(System.in);
			id = scanner.nextLine().trim();
			System.out.println("Please enter the new name of the product :");
			scanner = new Scanner(System.in);
			name = scanner.nextLine().trim();
			System.out.println("Please enter the new price of the product :");
			try {
				inputProcessor = new IntegerInputProcessor(reader);
				price = inputProcessor.process();
				System.out.println("Please enter the new expiration date of the product :");
				scanner = new Scanner(System.in);
				expirationDate = scanner.nextLine().trim();
				Product product = new Product(name, price, expirationDate);
				App.updateProductById(id, productCollection, product);
				System.out.println("Product successfully updated");
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			UserContexte.setUsername(email);
			writingLogger.info("Update product");
			
			break;
			
		}
	}
	
	public static void createUser(String name, String email, String password, int age) {
		User user = new User(name, email, password, age);
		App.insertUser(user, userCollection);
	}
	
	public static void setupReadingLogger() {
		readingLogger.setLevel(Level.ALL);
	    readingLogger.setUseParentHandlers(false); 

	    try {
	        FileHandler fh = new FileHandler("readingLogs.log", true);
	        fh.setFormatter(new JsonFormatteur());  // Utilisation du JsonFormatter
	        fh.setLevel(Level.FINE);
	        readingLogger.addHandler(fh);

	    } catch (java.io.IOException e) {
	        readingLogger.log(Level.SEVERE, "file logger not working", e);
	    }		     
    }
	
	public static void setupWritingLogger() {
		writingLogger.setLevel(Level.ALL);
		writingLogger.setUseParentHandlers(false); 

	    try {
	        FileHandler fh = new FileHandler("writingLogs.log", true);
	        fh.setFormatter(new JsonFormatteur());  // Utilisation du JsonFormatter
	        fh.setLevel(Level.FINE);
	        writingLogger.addHandler(fh);

	    } catch (java.io.IOException e) {
	    	writingLogger.log(Level.SEVERE, "file logger not working", e);
	    }		     
    }
	
	public static void setupPriceLogger() {
		priceLogger.setLevel(Level.ALL);
		priceLogger.setUseParentHandlers(false); 

	    try {
	        FileHandler fh = new FileHandler("expensiveProdLog.log", true);
	        fh.setFormatter(new JsonFormatteur());  // Utilisation du JsonFormatter
	        fh.setLevel(Level.FINE);
	        priceLogger.addHandler(fh);

	    } catch (java.io.IOException e) {
	    	priceLogger.log(Level.SEVERE, "file logger not working", e);
	    }		     
    }
}






