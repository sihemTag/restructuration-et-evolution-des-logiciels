package application;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;

import org.bson.types.ObjectId;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Sorts;

import models.Product;
import models.User;

public class App {
    
	public static void insertUser(User user,MongoCollection<Document> userCollection) {
		Document user1 = new Document("name", user.getName())
        		.append("age", user.getAge())
        		.append("email", user.getEmail())
        		.append("password", user.getPassword());
				
        userCollection.insertOne(user1);
	}
	
	public static boolean existUser(String email, MongoCollection<Document> userCollection) {
		Document document = (Document) userCollection.find(new Document("email", email)).first();
		if(document != null) {
            return true;
        }
		return false;
	}
	
	public static void insertProduct(Product product, MongoCollection<Document> productCollection) {
		Document product1 = new Document("name", product.getName())       		
        		.append("price", product.getPrice())
        		.append("expirationDate", product.getExpirationDate());
		
        productCollection.insertOne(product1);
	}
	
	public static void displayProducts(MongoCollection<Document> productCollection) {
		 FindIterable<Document> documents = productCollection.find();
		 MongoCursor<Document> cursor = documents.iterator();
		 while (cursor.hasNext()) {
             Document document = cursor.next();
             System.out.println(document.toJson());
         }
	}
	
	public static void getProductById(String id, MongoCollection<Document> productCollection) {
		ObjectId objectId = new ObjectId(id);
		Document document = (Document) productCollection.find(new Document("_id", objectId)).first();
		if(document != null) {
            System.out.println("Product found: " + document.toJson());
        } else {
            System.err.println("Product not found for ID: " + id);
        }
	}
	
	public static void deleteProductById(String id, MongoCollection<Document> productCollection) {
		ObjectId objectId = new ObjectId(id);
		Document document = (Document) productCollection.find(new Document("_id", objectId)).first();
		if(document != null) {
            productCollection.deleteOne(document);
        } else {
            System.err.println("Product not found for ID: " + id);
        }
	}
	
	public static void updateProductById(String id, MongoCollection<Document> productCollection, Product product) {
		ObjectId objectId = new ObjectId(id);
		Document document = (Document) productCollection.find(new Document("_id", objectId)).first();
		if(document != null) {
			Document replacementDocument = new Document("_id", objectId)
                    .append("name", product.getName())
                    .append("price", product.getPrice())
                    .append("expirationDate", product.getExpirationDate());

			productCollection.replaceOne(new Document("_id", objectId), replacementDocument);
        } else {
            System.err.println("Product not found for ID: " + id);
        }
	}
	
	public static List<Document> getTopExpensiveProducts(int limit,MongoCollection<Document> productCollection) {
        List<Document> products = new ArrayList<>();   
        productCollection.find()
                      .sort(Sorts.descending("price"))
                      .limit(limit)
                      .into(products);
        return products;
    }
}
