package application;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;
import org.json.JSONObject;

public class JsonFormatteur extends Formatter {
	
	private static final SimpleDateFormat dateFormat = 
	        new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Override
    public String format(LogRecord record) {
        JSONObject json = new JSONObject();
        json.put("message", record.getMessage());
        json.put("userName", UserContexte.getUsername());
        json.put("level", record.getLevel());
        json.put("logger_name", record.getLoggerName());   
        
     // Convertir timeMillis en timestamp formaté
        String formattedDate = dateFormat.format(new Date(record.getMillis()));
        json.put("timestamp", formattedDate);    

        return json.toString() + System.lineSeparator();
    }
}