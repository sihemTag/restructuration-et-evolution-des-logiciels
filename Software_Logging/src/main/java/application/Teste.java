package application;

import java.util.logging.*;


public class Teste {

    private final static Logger readingLogger = Logger.getLogger(Teste.class.getName() + ".reading");
    private final static Logger writingLogger = Logger.getLogger(Teste.class.getName() + ".writing");

    public static void main(String[] args) {
        setupReadingLogger();
        setupWritingLogger();

        getProducts("apple");
        addProducts("banana");
    }

    private static void setupReadingLogger() {
        //LogManager.getLogManager().reset();
        readingLogger.setLevel(Level.ALL);
        readingLogger.setUseParentHandlers(false); 

        try {
            FileHandler fh = new FileHandler("readingProfil.log", true);
            fh.setFormatter(new SimpleFormatter());
            fh.setLevel(Level.FINE);
            readingLogger.addHandler(fh);

        } catch (java.io.IOException e) {
            readingLogger.log(Level.SEVERE, "file logger not working", e);
        }
    }

    private static void setupWritingLogger() {
        //LogManager.getLogManager().reset();
        writingLogger.setLevel(Level.ALL);
        writingLogger.setUseParentHandlers(false); 

        try {
            FileHandler fh = new FileHandler("writingProfil.log", true);
            fh.setFormatter(new SimpleFormatter());
            fh.setLevel(Level.FINE);
            writingLogger.addHandler(fh);

        } catch (java.io.IOException e) {
            writingLogger.log(Level.SEVERE, "file logger not working", e);
        }
    }

    private static void getProducts(String name) {
        readingLogger.info("get product " + name);
    }

    private static void addProducts(String name) {
        writingLogger.info("add product " + name);
    }
}
