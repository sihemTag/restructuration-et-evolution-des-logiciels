package application;

import java.io.*;
import java.nio.file.*;
import java.util.*;
import org.json.*;
import com.google.gson.JsonObject;

public class LogAnalyser {
    public static void main(String[] args) throws IOException {
        Map<String, int[]> userActions = new HashMap<>();

        processLogFile("readingLogs.log", userActions, 0);
        processLogFile("writingLogs.log", userActions, 1);
        processLogFile("expensiveProdLog.log", userActions, 2);

        for (Map.Entry<String, int[]> entry : userActions.entrySet()) {
            JsonObject json = new JsonObject();
            json.addProperty("user", entry.getKey());
            json.addProperty("nb_reading", entry.getValue()[0]);
            json.addProperty("nb_writing", entry.getValue()[1]);
            json.addProperty("nb_search_expensive_prod", entry.getValue()[2]);
            System.out.println(json.toString());
        }
    }

    private static void processLogFile(String fileName, Map<String, int[]> userActions, int actionIndex) throws IOException {
        Path path = Paths.get(fileName);
        try (BufferedReader reader = Files.newBufferedReader(path)) {
            String line;
            while ((line = reader.readLine()) != null) {
                JSONObject logEntry = new JSONObject(line);
                String userName = logEntry.getString("userName");

                userActions.putIfAbsent(userName, new int[3]);
                userActions.get(userName)[actionIndex]++;
            }
        }
    }
}

