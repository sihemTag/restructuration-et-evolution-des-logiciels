package application;

public class UserContexte {
    private static final ThreadLocal<String> username = new ThreadLocal<>();

    public static void setUsername(String name) {
        username.set(name);
    }

    public static String getUsername() {
        return username.get();
    }
}

