package parser;

import org.eclipse.jdt.core.dom.SimpleName;

public class MethodInfo {
	
	private SimpleName name;
	private int nbLines;
	
	public MethodInfo(SimpleName name, int nbLines) {
		super();
		this.name = name;
		this.nbLines = nbLines;
	}

	public SimpleName getName() {
		return name;
	}

	public void setName(SimpleName name) {
		this.name = name;
	}

	public int getNbLines() {
		return nbLines;
	}

	public void setNbLines(int nbLines) {
		this.nbLines = nbLines;
	}
	
	

}
