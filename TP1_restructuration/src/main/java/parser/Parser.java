package parser;

import java.io.BufferedReader;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.FileUtils;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;

public class Parser {
	
	//public static final String projectPath = "C:\\Users\\SCD UM\\Downloads\\testProject";
	public static final String projectPath = "C:\\Users\\SCD UM\\eclipse-workspace\\testAnalyse";
	public static final String projectSourcePath = projectPath + "\\src";
	public static final String jrePath = "C:\\Program Files\\Java\\jre1.8.0_51\\lib\\rt.jar";

	public static void main(String[] args) throws IOException {

				
	}
	

	// read all java files from specific folder
	public static ArrayList<File> listJavaFilesForFolder(final File folder) {
		ArrayList<File> javaFiles = new ArrayList<File>();
		for (File fileEntry : folder.listFiles()) {
			if (fileEntry.isDirectory()) {
				javaFiles.addAll(listJavaFilesForFolder(fileEntry));
			} else if (fileEntry.getName().contains(".java")) {
				// System.out.println(fileEntry.getName());
				javaFiles.add(fileEntry);
			}
		}

		return javaFiles;
	}

	// create AST
	public static CompilationUnit parse(char[] classSource) {
		ASTParser parser = ASTParser.newParser(AST.JLS4); // java +1.6
		parser.setResolveBindings(true);
		parser.setKind(ASTParser.K_COMPILATION_UNIT);
 
		parser.setBindingsRecovery(true);
 
		Map options = JavaCore.getOptions();
		parser.setCompilerOptions(options);
 
		parser.setUnitName("");
 
		String[] sources = { projectSourcePath }; 
		String[] classpath = {jrePath};
 
		parser.setEnvironment(classpath, sources, new String[] { "UTF-8"}, true);
		parser.setSource(classSource);
		
		return (CompilationUnit) parser.createAST(null); // create and parse
	}
	
	//nombre de methodes dans l'appli
	public static int countMethods(CompilationUnit parse) {
		MethodDeclarationVisitor visitor = new MethodDeclarationVisitor();
		parse.accept(visitor);

		return visitor.getMethods().size();
	}

	// navigate variables inside method
	public static void printVariableInfo(CompilationUnit parse) {

		MethodDeclarationVisitor visitor1 = new MethodDeclarationVisitor();
		parse.accept(visitor1);
		for (MethodDeclaration method : visitor1.getMethods()) {

			VariableDeclarationFragmentVisitor visitor2 = new VariableDeclarationFragmentVisitor();
			method.accept(visitor2);

			for (VariableDeclarationFragment variableDeclarationFragment : visitor2
					.getVariables()) {
				System.out.println("variable name: "
						+ variableDeclarationFragment.getName()
						+ " variable Initializer: "
						+ variableDeclarationFragment.getInitializer());
			}

		}
	}
	
	// navigate method invocations inside method
		public static void printMethodInvocationInfo(CompilationUnit parse) {

			MethodDeclarationVisitor visitor1 = new MethodDeclarationVisitor();
			parse.accept(visitor1);
			for (MethodDeclaration method : visitor1.getMethods()) {

				MethodInvocationVisitor visitor2 = new MethodInvocationVisitor();
				method.accept(visitor2);

				for (MethodInvocation methodInvocation : visitor2.getMethods()) {
					/*System.out.println("method " + method.getName() + " invoc method "
							+ methodInvocation.getName());*/
					String fullyQualifiedName = methodInvocation.resolveMethodBinding().getDeclaringClass().getQualifiedName();

					// Split the fully qualified name by periods to get the parts
					String[] parts = fullyQualifiedName.split("\\.");

					// The class name is the last part of the split array
					String classe = parts[parts.length - 1];
					System.out.println(classe);
				}

			}
		}
		
		//calculer le nombre de ligne dans un fichier
		public static int countLines(File file) {
	        int lineCount = 0;
	        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
	            String line;
	            while ((line = br.readLine()) != null) {
	                lineCount++;
	            }
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	        return lineCount;
	    }
		
		
		public static int countPackages(File folder) {
		    int packageCount = 0;
		    for (File fileEntry : folder.listFiles()) {
		        if (fileEntry.isDirectory()) {
		            if (containsJavaFiles(fileEntry)) {
		                packageCount++;
		            }
		            packageCount += countPackages(fileEntry);
		        }
		    }
		    return packageCount;
		}

		public static boolean containsJavaFiles(File folder) {
		    for (File fileEntry : folder.listFiles()) {
		        if (fileEntry.isFile() && fileEntry.getName().endsWith(".java")) {
		            return true;
		        }
		    }
		    return false;
		}
		
	  public static int countAttributes(CompilationUnit parse) {  
		  FieldDeclarationVisitor visitor = new FieldDeclarationVisitor();
		  parse.accept(visitor);
			
		  return visitor.getFields().size();
	    }
	  
	  
	  public static String extractClassName(CompilationUnit parse) {
		  if(!parse.types().isEmpty()) {
	        TypeDeclaration typeDeclaration = (TypeDeclaration) parse.types().get(0);
	        return typeDeclaration.getName().getFullyQualifiedName();
	      }
		  return "";
	    }
	  
	  public static String extractFullClassName(CompilationUnit parse) {
		    if (parse.getPackage() != null) {
		        String packageName = parse.getPackage().getName().getFullyQualifiedName();
		        if (!parse.types().isEmpty()) {
		            TypeDeclaration typeDeclaration = (TypeDeclaration) parse.types().get(0);
		            String className = typeDeclaration.getName().getFullyQualifiedName();
		            return packageName + "." + className;
		        }
		    }
		    return "";
		}
	  
	  public static String possedeXmethodes(int x, CompilationUnit parse) {
		  int nbMethodes = countMethods(parse);
		  if(nbMethodes>x) return extractClassName(parse);
		  return "Aucune classe trouvée";
	  }
	  
	  public static void printNbLigneParMethod(CompilationUnit parse, String className	) {
			MethodDeclarationVisitor visitor = new MethodDeclarationVisitor();
			parse.accept(visitor);
			List<MethodInfo> listMethods = new ArrayList<MethodInfo>();

			for (MethodDeclaration method : visitor.getMethods()) {
				int startPos = method.getStartPosition();
				int endPos = startPos + method.getLength();
				
				int startLine = parse.getLineNumber(startPos);
		        int endLine = parse.getLineNumber(endPos);
		        int nbLines = endLine - startLine +1;
		        
		        MethodInfo methodInfo = new MethodInfo(method.getName(), nbLines);
		        listMethods.add(methodInfo);
			}
			
			//ordonner les methodes selon le nombre de lignes
	        listMethods.sort(Comparator.comparingInt(MethodInfo::getNbLines).reversed());
	        
	        //selectionner 10% des top methodes
	        double tenPercent = 0.1 * (double)(visitor.getMethods().size());
	        List<MethodInfo> topTenPercentMethods = listMethods.subList(0, (int) tenPercent);
	        
	        for(MethodInfo meth: topTenPercentMethods) {
	        	System.out.println("	Classe: "+className+":");
	        	System.out.println("		Méthode : "+ meth.getName()+ ", nombre de lignes: "+ meth.getNbLines());
	        }
		}
	  
	  //nb lines par method
	  public static int nbLigneParMethod(MethodDeclaration method, CompilationUnit parse) {
			int startPos = method.getStartPosition();
			int endPos = startPos + method.getLength();
			
			int startLine = parse.getLineNumber(startPos);
	        int endLine = parse.getLineNumber(endPos);
	        int nbLines = endLine - startLine +1;  
	        
	        return nbLines;
		}
	  
	  public static int nbMaxParam(CompilationUnit parse) {
			MethodDeclarationVisitor visitor = new MethodDeclarationVisitor();
			parse.accept(visitor);
			int max = 0;
			int nbParam;
			
			for (MethodDeclaration method : visitor.getMethods()) {
				nbParam = method.parameters().size();
				if(nbParam>max) max = nbParam;
			}
			return max;	
	  }
	  
	  public static List<ClassMethods> topTenPercentMethods(List<File> javaFiles){
			List<ClassMethods> classList = new ArrayList<>();
			String nameClasse;
			
			for (File fileEntry : javaFiles) {
				String content;
				try {
					content = FileUtils.readFileToString(fileEntry);
					CompilationUnit parse = Parser.parse(content.toCharArray());
					
					nameClasse = Parser.extractClassName(parse);
					if(!nameClasse.equals("")) { 
						ClassMethods classe = new ClassMethods(nameClasse, Parser.countMethods(parse), Parser.countAttributes(parse));
						classList.add(classe);
					}
					
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			//ordonner les classe selon le nb de methodes
	        classList.sort(Comparator.comparingInt(ClassMethods::getNbMethods).reversed());
			
			//selectionner 10% des top classes
	        double tenPercent = 0.1 * (double)javaFiles.size();
	        List<ClassMethods> topTenPercentMeth = classList.subList(0, (int) tenPercent);
	        
	        return topTenPercentMeth;
	  }
	  
	  public static List<ClassMethods> topTenPercentAttributes(List<File> javaFiles){
		  	List<ClassMethods> classList = new ArrayList<>();
			String nameClasse;
			
			for (File fileEntry : javaFiles) {
				String content;
				try {
					content = FileUtils.readFileToString(fileEntry);
					CompilationUnit parse = Parser.parse(content.toCharArray());
					
					nameClasse = Parser.extractClassName(parse);
					if(!nameClasse.equals("")) { 
						ClassMethods classe = new ClassMethods(nameClasse, Parser.countMethods(parse), Parser.countAttributes(parse));
						classList.add(classe);
					}
					
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			//ordonner les classe selon le nb d'attributs
	        classList.sort(Comparator.comparingInt(ClassMethods::getNbAttributes).reversed());
			
	      //selectionner 10% des top classes
	        double tenPercent = 0.10 * (double)javaFiles.size();
	        List<ClassMethods> topTenPercentAttr = classList.subList(0, (int) tenPercent);
	        
	        return topTenPercentAttr; 
	  }
	  
	  public static List<MethodDeclaration> getMethodesFromClasse(CompilationUnit parse){
		  MethodDeclarationVisitor visitor = new MethodDeclarationVisitor();
		  parse.accept(visitor);
		  return visitor.getMethods();
			
	  }
	 

}
