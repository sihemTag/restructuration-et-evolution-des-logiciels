package parser;

import java.util.Objects;

import org.eclipse.jdt.core.dom.CompilationUnit;

public class ClassMethods{
	
	String name;
	int nbMethods;
	int nbAttributes;
	
	public ClassMethods(String name, int nbMethods, int nbAttributes) {
		super();
		this.name = name;
		this.nbMethods = nbMethods;
		this.nbAttributes = nbAttributes;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNbMethods() {
		return nbMethods;
	}

	public void setNbMethods(int nbMethods) {
		this.nbMethods = nbMethods;
	}

	/*@Override
	public int compareTo(ClassMethods o) {
		return Integer.compare(o.nbMethods, this.nbMethods);
	}*/

	public int getNbAttributes() {
		return nbAttributes;
	}

	public void setNbAttributes(int nbAttributes) {
		this.nbAttributes = nbAttributes;
	}
	
	
	
	
	
}
