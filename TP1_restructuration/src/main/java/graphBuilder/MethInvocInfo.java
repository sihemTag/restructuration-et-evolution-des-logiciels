package graphBuilder;

import org.eclipse.jdt.core.dom.SimpleName;

public class MethInvocInfo {
	
	private String callerName;
	private String callerClassName;
	private SimpleName calleeName;
	private String calleeClassName;
	
	public MethInvocInfo(String callerName, String callerClassName, SimpleName calleeName, String calleeClassName) {
		super();
		this.callerName = callerName;
		this.callerClassName = callerClassName;
		this.calleeName = calleeName;
		this.calleeClassName = calleeClassName;
	}

	public String getCallerName() {
		return callerName;
	}

	public void setCallerName(String callerName) {
		this.callerName = callerName;
	}

	public String getCallerClassName() {
		return callerClassName;
	}

	public void setCallerClassName(String callerClassName) {
		this.callerClassName = callerClassName;
	}

	public SimpleName getCalleeName() {
		return calleeName;
	}

	public void setCalleeName(SimpleName calleeName) {
		this.calleeName = calleeName;
	}

	public String getCalleeClassName() {
		return calleeClassName;
	}

	public void setCalleeClassName(String calleClassName) {
		this.calleeClassName = calleClassName;
	}
	

	
}
