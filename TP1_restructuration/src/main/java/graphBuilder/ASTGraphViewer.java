package graphBuilder;

import java.io.File;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.SwingConstants;

import org.apache.commons.io.FileUtils;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;

import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.util.mxRectangle;
import com.mxgraph.view.mxGraph;
import com.mxgraph.layout.hierarchical.mxHierarchicalLayout;
import com.mxgraph.model.mxCell;


import parser.MethodDeclarationVisitor;
import parser.MethodInvocationVisitor;
import parser.Parser;

public class ASTGraphViewer {
	
	 public static void createGraphe(ArrayList<File> javaFiles) {
		 ArrayList<MethodNode> methodNodes = new ArrayList<MethodNode>();
			ArrayList<String> classes = new ArrayList<String>();
			ArrayList<MethInvocInfo> methInvocInfoList = new ArrayList<MethInvocInfo>();
			
	        // Créez un graphe mxGraph
	        mxGraph graph = new mxGraph();
	        Object parent = graph.getDefaultParent();

	        // Commencez à ajouter des nœuds au graphe (c'est ici que vous ajoutez les éléments de votre AST)
	        graph.getModel().beginUpdate();
	        try {
	        	for (File fileEntry : javaFiles) {
	    		    try {
	    		        String content = FileUtils.readFileToString(fileEntry);
	    		        CompilationUnit parse = Parser.parse(content.toCharArray());

	    		        MethodDeclarationVisitor methodDeclarationVisitor = new MethodDeclarationVisitor();
	    			    parse.accept(methodDeclarationVisitor);
	    			    String className = Parser.extractFullClassName(parse);  
	    			    	    		

	    			    // Iterate over method declarations
	    			    for (MethodDeclaration methodDeclaration : methodDeclarationVisitor.getMethods()) {
	    			        // Extract method information (name, parameters, etc.)   		
	    			        String methodName = methodDeclaration.getName().getIdentifier(); 
	    			        MethodNode methNode = new MethodNode(methodName, className);
	    			        methodNodes.add(methNode);
	    			        
	    			        //ajouter methodName dans une liste   
	    			        
	    			        MethodInvocationVisitor visitor2 = new MethodInvocationVisitor();
	    					methodDeclaration.accept(visitor2);
	    			        for (MethodInvocation methodInvocation : visitor2.getMethods()) {
	    						IMethodBinding binding = methodInvocation.resolveMethodBinding();
	    						if(binding!=null) {
	    							String fullyQualifiedName = methodInvocation.resolveMethodBinding().getDeclaringClass().getQualifiedName();

		    						// Split the fully qualified name by periods to get the parts
		    						String[] parts = fullyQualifiedName.split("\\.");
	
		    						// The class name is the last part of the split array
		    						String invocMethClass = parts[parts.length - 1];
		    						
		    						MethInvocInfo  methInvocInfo = new MethInvocInfo(methodName, className, methodInvocation.getName(), invocMethClass);
		    						methInvocInfoList.add(methInvocInfo);
	    						}
	    			        }
	    			    }	    			 
	    			    
	    			    if(!className.equals("")) classes.add(className);
	    			    
	    		    } catch (IOException e) {
	    		        e.printStackTrace();
	    		    }
	    		}    
	            // Ajoutez d'autres nœuds et arêtes selon votre AST
	        	int x = 20; // Coordonnée x initiale
		        int y = 20; // Coordonnée y initiale
	        	for (String className : classes) {
	        	    Object node1 = graph.insertVertex(parent, null, className, x, y, 200, 30);
	        	 
	        	    mxRectangle size = graph.getPreferredSizeForCell(node1);     	 
	        	    graph.resizeCell(node1, size);
	        	            	 
	        	    mxCell cell = (mxCell) node1;	        	 
	        	    cell.setStyle("fillColor=#ff7866");
	        	    
	        	    x += 100;		            
	        	}
	        	y += 200;
	        	x=20;
	        	for (MethodNode meth : methodNodes) {
	        	    Object node = graph.insertVertex(parent, null, "<"+meth.getClassName()+","+meth.getName()+">", x, y, 200, 30);
	        	    mxRectangle size = graph.getPreferredSizeForCell(node);     	 
	        	    graph.resizeCell(node, size);
	        	    
	        	    x += 100;     
	        	    
	        	    for (Object cell : graph.getChildCells(parent)) {
	        	        if (cell instanceof mxCell) {
	        	            mxCell mxCell = (mxCell) cell;
	        	            Object value = mxCell.getValue();

	        	            // Vérifiez si la cellule est un nœud et si la valeur correspond au libellé recherché
	        	            if (mxCell.isVertex() && value != null && value.toString().equals(meth.getClassName())) {
	        	            	 Object edge = graph.insertEdge(parent, null, "",cell, node);
	        	            	// Définissez la couleur de l'arête en utilisant le style
	        	            	 mxCell edgeCell = (mxCell) edge;
	        	            	 edgeCell.setStyle("strokeColor=#ff7866");
	        	            }
	        	        }
	        	    }	    
	        	 }
	        	
	        	for(MethInvocInfo methInv : methInvocInfoList) {
	        		mxCell callerNode = null, calleeNode=null;
	        		Boolean callerFound= false, calleeFound = false;
	        		Object node1=null,node2 = null;
	      
	        		//chercher caller
	        		for (Object cell : graph.getChildCells(parent)) {
	        	        if (cell instanceof mxCell) {
	        	            mxCell mxCell = (mxCell) cell;
	        	            Object value = mxCell.getValue();
	        	            
	        	            if (mxCell.isVertex() && value != null && value.toString().equals("<"+methInv.getCallerClassName()+","+methInv.getCallerName()+">")) {
	        	            	callerNode = mxCell;	        	          
	        	            	callerFound=true;
	        	            	break;
	        	            }	        	
	        	        }        	       
	        		}
	        		if(!callerFound) {
	        			 node1 = graph.insertVertex(parent, null, "<"+methInv.getCallerClassName()+","+methInv.getCallerName()+">", x, y, 200, 30);
	        			 mxRectangle size = graph.getPreferredSizeForCell(node1);     	 
	 	        	     graph.resizeCell(node1, size);
	        		}
	        		
	        		
	        		//chercher callee
	        		for (Object cell : graph.getChildCells(parent)) {
	        	        if (cell instanceof mxCell) {
	        	            mxCell mxCell = (mxCell) cell;
	        	            Object value = mxCell.getValue();
	        	            
	        	            if (mxCell.isVertex() && value != null && value.toString().equals("<"+methInv.getCalleeClassName()+","+methInv.getCalleeName()+">")) {
	        	            	calleeNode = mxCell;	        	          
	        	            	calleeFound=true;
	        	            	break;
	        	            }	        	
	        	        }        	       
	        		}
	        		if(!calleeFound) {
	        			 node2 = graph.insertVertex(parent, null, "<"+methInv.getCalleeClassName()+","+methInv.getCalleeName()+">", x, y, 200, 30);
	        			 mxRectangle size = graph.getPreferredSizeForCell(node2);     	 
	 	        	    graph.resizeCell(node2, size);
	        		}
	        		if(callerFound && calleeFound) {
	        			graph.insertEdge(parent, null, "",callerNode, calleeNode);
	        		}else if(callerFound && !calleeFound) {
	        			graph.insertEdge(parent, null, "",callerNode, node2);
	        		}else {
	        			graph.insertEdge(parent, null, "",node1, calleeNode);
	        		}
	        		
	        		
	        	}
	        	
	        	
	        }finally {
	            graph.getModel().endUpdate();
	        }
	        
	        
	     // Créez une instance de mxHierarchicalLayout et configurez-la
	        mxHierarchicalLayout layout = new mxHierarchicalLayout(graph);
	        layout.setOrientation(SwingConstants.WEST);
	        layout.setIntraCellSpacing(10);

	        // Exécutez la mise en page hiérarchique
	        layout.execute(parent);
	                
	        
	        // Créez un composant graphique pour afficher le graphe
		    mxGraphComponent graphComponent = new mxGraphComponent(graph);
		
		    // Créez une fenêtre pour afficher le composant graphique
		    JFrame frame = new JFrame();
		    frame.getContentPane().add(graphComponent);
		    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		    frame.setSize(1000, 1000);
		    frame.setVisible(true);
		 
	 }
}
