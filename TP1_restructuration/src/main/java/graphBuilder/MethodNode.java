package graphBuilder;

import java.util.ArrayList;

public class MethodNode {
	private String name;
	private String className;
	private int nbLines;
	
	public MethodNode(String name, String className) {
		super();
		this.name = name;
		this.className=className;
	}
	
	public MethodNode(String name, int nbLines, String className) {
		super();
		this.name = name;
		this.nbLines = nbLines;
		this.className=className;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNbLines() {
		return nbLines;
	}

	public void setNbLines(int nbLines) {
		this.nbLines = nbLines;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}
	

}
