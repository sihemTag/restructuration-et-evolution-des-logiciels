package cli;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.io.FileUtils;
import org.eclipse.jdt.core.dom.CompilationUnit;

import graphBuilder.ASTGraphViewer;
import parser.ClassMethods;
import parser.Parser;

public class UserCLI extends AbstractMain{
	
	public static IntegerInputProcessor inputProcessor;
	public static  String projectSourcePath="";
	public static final String jrePath = "C:\\Program Files\\Java\\jre1.8.0_51\\lib\\rt.jar";
	
	@SuppressWarnings("resource")
	public static void main(String[] args)  {
		BufferedReader inputReader;
		String userInput = "";
		UserCLI main = new UserCLI();
		
		try {
			inputReader = new BufferedReader(new InputStreamReader(System.in));
			Scanner scanner; 
			System.out.println("Veuillez saisir un chemin vers un code source : ");
			scanner = new Scanner(System.in);
			projectSourcePath = scanner.nextLine().trim();
			
			while(!projectSourcePath.endsWith("\\src")) {
				System.err.println("Veuillez saisir un chemin valide. Le chemin doit se terminer par '\\src'.");
				scanner = new Scanner(System.in);
				projectSourcePath = scanner.nextLine().trim();
			}
			
			// read java files
			final File folder = new File(projectSourcePath);
			ArrayList<File> javaFiles = Parser.listJavaFilesForFolder(folder);
	        
			//afficher le menu a l'utilisateur
			do {
				main.menu();
				userInput = inputReader.readLine();
				main.processUserInput(inputReader,userInput, javaFiles, folder);
				Thread.sleep(2000);
			} while(!userInput.equals(QUIT));
		}catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
			
	}


	@Override
	protected void menu() {
		StringBuilder builder = new StringBuilder();
		builder.append(QUIT+". Quit");
		builder.append("\n1. Nombre de classes de l'application.");
		builder.append("\n2. Nombre de lignes de code dans l'application.");
		builder.append("\n3. Nombre total de méthodes de l’application.");
		builder.append("\n4. Nombre total de packages de l'application.");
		builder.append("\n5. Nombre moyen de méthodes par classe.");
		builder.append("\n6. Nombre moyen de lignes de code par méthode.");
		builder.append("\n7. Nombre moyen d'attributs par classe.");
		builder.append("\n8. Les 10% des classes qui possèdent le plus grand nombre de méthodes.");		
		builder.append("\n9. Les 10% des classes qui possèdent le plus grand nombre d'attributs.");	
		builder.append("\n10. Les classes qui font partie en même temps des deux catégories précédentes.");	
		builder.append("\n11. Les classes possédant plus de x methodes.");	
		builder.append("\n12. Les 10% des méthodes qui possèdent le plus grand nombre de lignes de code (par classe).");
		builder.append("\n13. Le nombre maximal de paramètres par rapport à toutes les méthodes de l'application.");
		builder.append("\n14. Afficher le graphe d'appel.");
		
		System.out.println(builder);
	}
	
	private void processUserInput(BufferedReader reader, String userInput, ArrayList<File> javaFiles, File folder) {
		int nbLines, nbMethods,nbTotalAttributes, x,maxParam, nbParams;; 
		List<String> possedXmethod=new ArrayList<String>() ;
		String classePossedXMethod;
		
		
		switch(userInput) {
		
		case QUIT :
			System.out.println("A bientôt!");
			return;
			
		case "1":
			System.out.println("L'application contient "+javaFiles.size()+" classes.");
			break;
			
		case "2" :
			nbLines=0;
			for (File fileEntry : javaFiles) {
				String content;
				try {
					content = FileUtils.readFileToString(fileEntry);
					CompilationUnit parse = Parser.parse(content.toCharArray());
					nbLines+= Parser.countLines(fileEntry);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			System.out.println("Nombre de lignes de code dans l'application est : "+nbLines);
			break;
			
		case "3" :
			nbMethods=0;
			for (File fileEntry : javaFiles) {
				String content;
				try {
					content = FileUtils.readFileToString(fileEntry);
					CompilationUnit parse = Parser.parse(content.toCharArray());
					nbMethods+= Parser.countMethods(parse);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			System.out.println("Nombre total de méthodes dans l'application est : "+nbMethods);
			break;
			
		case "4":
			System.out.println("Nombre total de packages dans l'application est: "+Parser.countPackages(folder));
			break;
			
		case "5":
			nbMethods=0;
			for (File fileEntry : javaFiles) {
				String content;
				try {
					content = FileUtils.readFileToString(fileEntry);
					CompilationUnit parse = Parser.parse(content.toCharArray());
					nbMethods+= Parser.countMethods(parse);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			float nbMoyenMehtod =  (float)nbMethods / (float)javaFiles.size();
			System.out.println("Nombre moyen de methodes par classe est: "+nbMoyenMehtod);
			break;
		
		case "6":
			nbMethods=0;
			nbLines=0;
			for (File fileEntry : javaFiles) {
				String content;
				try {
					content = FileUtils.readFileToString(fileEntry);
					CompilationUnit parse = Parser.parse(content.toCharArray());
					nbMethods+= Parser.countMethods(parse);
					nbLines+= Parser.countLines(fileEntry);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			float nbMoyenLigneCodeParMethod =  (float)nbLines /(float)nbMethods;
			System.out.println("Nombre moyen de lignes de code par méthode est: "+nbMoyenLigneCodeParMethod);
			break;
			
		case "7":
			nbTotalAttributes=0;
			for (File fileEntry : javaFiles) {
				String content;
				try {
					content = FileUtils.readFileToString(fileEntry);
					CompilationUnit parse = Parser.parse(content.toCharArray());
					nbTotalAttributes+= Parser.countAttributes(parse);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			float nbMoyenAttributParClasse =  (float)nbTotalAttributes /(float)javaFiles.size();
			System.out.println("Nombre moyen d'attributs par classe est: "+Math.round(nbMoyenAttributParClasse));
			break;
			
		case "8":
			List<ClassMethods> topTenPercentMeth = Parser.topTenPercentMethods(javaFiles);
	        System.out.println("Les 10% des classes qui possèdent le plus grand	nombre de méthodes: ");
			for (ClassMethods clazz : topTenPercentMeth) {
	            System.out.println("	La classe: " + clazz.getName() + ", nombre de méthodes: " + clazz.getNbMethods());
	        }
			break;
			
		case "9":
			List<ClassMethods> topTenPercentAttr = Parser.topTenPercentAttributes(javaFiles);
	        System.out.println("Les 10% des classes qui possèdent le plus grand	nombre d'attributs: ");
			for (ClassMethods clazz : topTenPercentAttr) {
	            System.out.println("	La classe: " + clazz.getName() + ", nombre d'attributs: " + clazz.getNbAttributes());
	        }
			break;
			
		case "10":
			List<String> listNames1= new ArrayList<String>(), listNames2= new ArrayList<String>();
			List<ClassMethods> topMeth = Parser.topTenPercentMethods(javaFiles);
			List<ClassMethods> topAttr = Parser.topTenPercentAttributes(javaFiles);
			
			for (ClassMethods clazz : topMeth) {
				listNames1.add(clazz.getName());
			}
			
			for (ClassMethods clazz : topAttr) {
				listNames2.add(clazz.getName());
			}
			
			List<String> intersection = listNames1.stream()
	                .filter(listNames2::contains)
	                .distinct()
	                .toList(); 

			System.out.println("Les classes	qui	font partie	en même temps des deux catégories précédentes: ");
			for (String className : intersection) {
	            System.out.println("	La classe: " + className);
	        }
			break;
			
		case "11":
			System.out.println("Veuillez entrer le nombre de méthodes :");
			inputProcessor = new IntegerInputProcessor(reader);
			try {
				 x = inputProcessor.process();
				 possedXmethod = new ArrayList<String>();
				 for (File fileEntry : javaFiles) {
						String content;
						try {
							content = FileUtils.readFileToString(fileEntry);
							CompilationUnit parse = Parser.parse(content.toCharArray());
							classePossedXMethod = Parser.possedeXmethodes(x,parse);
							if(!classePossedXMethod.equals("Aucune classe trouvée")) {
								possedXmethod.add(classePossedXMethod);
							}
							
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				 System.out.println("Les classes possédant plus de "+x+" methodes : ");
					for(String s : possedXmethod) {
						System.out.println("	La classe: "+s+" possède "+x+" methodes.");
					}
			} catch (IOException e) {
				e.printStackTrace();
			}
			break;
			
		case "12":
			System.out.println("Les 10% des méthodes qui possèdent le plus grand nombre de lignes de code (par classe): ");
			for (File fileEntry : javaFiles) {
				String content, nomClasse;
				try {
					content = FileUtils.readFileToString(fileEntry);
					CompilationUnit parse = Parser.parse(content.toCharArray());
					nomClasse = Parser.extractClassName(parse);
					Parser.printNbLigneParMethod(parse,nomClasse);
				} catch (IOException e) {
					e.printStackTrace();
				}	
			}
			break;
			
		case "13" :
			maxParam=0;
			for (File fileEntry : javaFiles) {
				String content;
				try {
					content = FileUtils.readFileToString(fileEntry);
					CompilationUnit parse = Parser.parse(content.toCharArray());
					nbParams = Parser.nbMaxParam(parse);
					if(nbParams>maxParam) maxParam = nbParams;
				} catch (IOException e) {
					e.printStackTrace();
				}	
			}
			System.out.println("Le nombre maximal de paramètres par rapport à toutes les méthodes de la'application: " + maxParam) ;	
			break;	
			
		case "14":
			ASTGraphViewer.createGraphe(javaFiles);
			break;
		}
	}
}
